report zsp1_check_st_value.



data : p_value type int4.
zcls_sp1_static_con=>set_value( i_value = 5 ).


call function 'ZOO_DP_1'
  importing
    r_value = p_value.    " Doğal sayı

write :'Static sınıftan gelen', p_value.

skip .
"" singleton

   data(cl_object) = zcls_sp1_static_con=>factory( ).

   p_value = cl_object->get_value( ).

   write : 'Instance programın içinde' , p_value.

   skip .
   " fonskiyonda instance üzerinden alınan değer


   call function 'ZOO_DP_2'
     importing
       r_value = p_value
     .

     write : 'Fonksiyonda çağrılan instance gelen ',  p_value.
     skip .