*&---------------------------------------------------------------------*
*& Report  ZFP_FACADE_PATTERN
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*

report zfp_facade_pattern.

class lcl_data definition.
  public section.
    methods: constructor.
endclass.
*
interface lif_bank.
  methods: read_table,
    send_bank,
    get_result,
    get_limits.
endinterface.                    "lif_banka
*
class lcl_akbank definition.
  public section.
    interfaces: lif_bank.
endclass.                    "lcl_akbank
*
class lcl_garanti definition.
  public section.
    interfaces: lif_bank.
endclass.                    "lcl_garanti
*
class lcl_facade definition.
  public section.
    methods: process_operation importing iv_bank_type type char1.
endclass.                    "lcl_facade DEFINITION
*
class lcl_data implementation.
  method constructor.
    write: / 'Process Başladı'.
  endmethod.                    "constructor
endclass.                    "lcl_Data IMPLEMENTATION
*
class lcl_akbank implementation.
  method lif_bank~send_bank.
    write: / 'AKBANK veri dosyası ftp gönderildi'.
  endmethod.
  method    lif_bank~get_result.
    write: / 'AKBANK sonuç dosyası ftp okundu'.
  endmethod.
  method lif_bank~get_limits.
    write: / 'AKBANK limit dosyası  ftp okundu'.
  endmethod.
  method lif_bank~read_table.
    write: / 'AKBANK Verisi okundu'.
  endmethod.                    "lif_write~write_Data
endclass.                    "lcl_write_alv IMPLEMENTATION
*
class lcl_garanti implementation.
  method lif_bank~send_bank.
    write: / 'Garanti veri dosyası Ws üzerinden gönderildi'.
  endmethod.
  method    lif_bank~get_result.
    write: / 'Garanti sonuç dosyası Ws üzerinden okundu'.
  endmethod.
  method lif_bank~get_limits.
    write: / 'Garanti limit dosyası Ws üzerinden okundu'.
  endmethod.
  method lif_bank~read_table.
    write: / 'Garanti Verisi okundu'.
  endmethod.                   "lif_write~write_Data
endclass.                    "lcl_write_log IMPLEMENTATION
*
class lcl_facade implementation.
  method process_operation.
    data: lo_data type ref to lcl_data.
    create object lo_data.

"" sınıfımızı buldutakn sonra üst tasarım şablounumuz olan interface ile eşliyoruz
 " bu interface altında aynı methodları oluşturmul olduk

    data: lo_bank type ref to lif_bank.
    if iv_bank_type = 'A'.
      create object lo_bank type lcl_akbank.
    else.
      create object lo_bank type lcl_garanti.
    endif.

    lo_bank->read_table( ).
    lo_bank->send_bank( ).
    lo_bank->get_result( ).
    lo_bank->get_limits( ).

  endmethod.
endclass.

start-of-selection.
  data: lo_facade type ref to lcl_facade.
  create object lo_facade.
  " öncelikli olarak müşterimiz hangi banka ile çalışıyor bunu bulduktan sonra
  " projess yapıcak sınıfımızı çağırıyoruz
  lo_facade->process_operation( iv_bank_type = 'G' ).