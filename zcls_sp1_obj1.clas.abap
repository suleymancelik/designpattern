class zcls_sp1_obj1 definition
  public
  final
  create public .

  public section.
    methods constructor.
    methods get_value
            returning value(r_value) type i.

  protected section.
  private section.
    data pb_value type i.

ENDCLASS.



CLASS ZCLS_SP1_OBJ1 IMPLEMENTATION.


  method constructor.

    pb_value = 2.

  endmethod.


  method get_value.
    r_value = pb_value.
  endmethod.
ENDCLASS.