class zcls_sp1_static_con definition
  public
  final
  create public .

  public section.

    class-methods set_value
      importing i_value type int4.

    class-methods get_value
      returning value(r_value) type int4.

    class-methods factory
      returning value(r_object) type ref to zcls_sp1_obj1.
    class-methods defactory.

    class-methods get_object
    returning value(r_object) type ref to zcls_sp1_obj1.


  private section.
    class-data i_value type int4 .
    class-data cl_instance type ref to zcls_sp1_obj1.

ENDCLASS.



CLASS ZCLS_SP1_STATIC_CON IMPLEMENTATION.


  method defactory.
    free zcls_sp1_static_con=>cl_instance.
  endmethod.


  method factory.

    if zcls_sp1_static_con=>cl_instance is initial.
      create object zcls_sp1_static_con=>cl_instance.
    endif.

    r_object = zcls_sp1_static_con=>cl_instance.

  endmethod.


  method get_object.
        r_object = zcls_sp1_static_con=>cl_instance.
  endmethod.


  method get_value.
    r_value = zcls_sp1_static_con=>i_value.
  endmethod.


  method set_value.
    zcls_sp1_static_con=>i_value = i_value.
  endmethod.
ENDCLASS.