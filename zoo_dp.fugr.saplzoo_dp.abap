*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZOO_DPTOP.                        " Global Data
  INCLUDE LZOO_DPUXX.                        " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZOO_DPF...                        " Subroutines
* INCLUDE LZOO_DPO...                        " PBO-Modules
* INCLUDE LZOO_DPI...                        " PAI-Modules
* INCLUDE LZOO_DPE...                        " Events
* INCLUDE LZOO_DPP...                        " Local class implement.
* INCLUDE LZOO_DPT99.                        " ABAP Unit tests